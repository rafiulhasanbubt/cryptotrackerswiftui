//
//  XMarkButtonView.swift
//  CryptoTrackerSwiftUI
//
//  Created by rafiul hasan on 15/10/21.
//

import SwiftUI

struct XMarkButtonView: View {
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        Button(action: {
            presentationMode.wrappedValue.dismiss()
        }, label: {
            Image(systemName: "xmark")
                .font(.headline)
        })
    }
}

struct XMarkButtonView_Previews: PreviewProvider {
    static var previews: some View {
        XMarkButtonView()
    }
}
