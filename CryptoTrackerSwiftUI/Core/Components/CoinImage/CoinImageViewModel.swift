//
//  CoinImageViewModel.swift
//  CryptoTrackerSwiftUI
//
//  Created by rafiul hasan on 10/10/21.
//

import Foundation
import SwiftUI
import Combine

class CoinImageViewModel: ObservableObject {
    @Published var image: UIImage? = nil
    @Published var isLoading: Bool = false
    
    private var coin: CoinModel
    private var dataService: CoinImageService
    private var cancellable = Set<AnyCancellable>()
    
    
    init(coin: CoinModel) {
        self.coin = coin
        self.dataService = CoinImageService(coin: coin)
        self.isLoading = true
        self.addSubscribers()
    }
    
    private func addSubscribers() {
        dataService.$image
            .sink{ [weak self] _ in
                    self?.isLoading = false
            } receiveValue: { [weak self] returnImage in
                self?.image = returnImage
            }
            .store(in: &cancellable)
    }
}
