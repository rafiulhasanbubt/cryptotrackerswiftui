//
//  UIApplication.swift
//  CryptoTrackerSwiftUI
//
//  Created by rafiul hasan on 5/10/21.
//

import Foundation
import SwiftUI

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
