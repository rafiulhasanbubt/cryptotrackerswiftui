//
//  String.swift
//  CryptoTrackerSwiftUI
//
//  Created by rafiul hasan on 5/10/21.
//

import Foundation

extension String {
    var removingHTMLOccurances: String {
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
}
