//
//  HapticManager.swift
//  CryptoTrackerSwiftUI
//
//  Created by rafiul hasan on 21/10/21.
//

import Foundation
import SwiftUI

class HapticManager {
    static private let generator = UINotificationFeedbackGenerator()
    static func notification(type: UINotificationFeedbackGenerator.FeedbackType) {
        generator.notificationOccurred(type)
    }
}
